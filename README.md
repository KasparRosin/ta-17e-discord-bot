# discord-bot homework

Create [Discord](https://discordapp.com/) bot.

> You are free to use any tool/language you want to create your bot!

- Fork this repo and make Merge Request with your code (to master branch).
   - If you want to host your code somewhere else, please create issue that has all the details.
- Bot need to be tested as much as possible.
   - Unit testing for logic and code linting is have to be!
- Bot need to be deployed by CI each time new code is pushed.

Things bot have to make:
 - Bot have to join server `test2019` ([https://discord.gg/38KSBt](https://discord.gg/38KSBt)), channel `general`.
 - Bot have to implement at-least following commands:
    - `!who` or bot name used in anywhere in text -> bot gives info about himself, including who created him and git commit hash for current version deployed.
    - `!help` or `!help!BOTNAME` - bot gives list of available commands
    - at-least one command that is unique to your bot (example bot has command `!chuck` that is making him to say one joke about Chuck Norris).

You are allowed to test your class mate bot's - if you managed to find any bug please let teacher know (bonus for that)! **Load testing is now allowed!**
 > random change

# https://discordapp.com/oauth2/authorize?client_id=577370896021585930&scope=bot&permissions=3136 