module.exports = {
	name: 'Help',
	description: 'Help!',
	execute(message) {
		const Discord = require('discord.js');
		const EmbedHelp = new Discord.RichEmbed()
			.setColor('#ffffff')
			.setAuthor(message.author.username, message.author.avatarURL)
			.addField('!WHO', 'Find out who made this epic bot')
			.addField('!TIMEZ <time>', 'Time Converter')
			.setTimestamp()
			.setFooter('Made By ZeroWidthWhiteSpace#9999', 'https://cdn.discordapp.com/avatars/506893172052459521/3214c24c381bf5dbe538606c40f24349.png?size=128');
		message.channel.send(EmbedHelp);
	},
};
