module.exports = {
	name: 'Timez',
	description: 'Timezone converter!',
	execute(message, args) {
		if (args === undefined || args[0].toLowerCase() !== "utc") {
			message.channel.reply('Please insert correct timezone.');
			return;
		}
		if (args[0].toLowerCase() === "utc") {
			if (args[1] == undefined) {
				message.channel.reply('Please insert time');
				return;
			}
			const moment = require('moment-timezone');
			const Discord = require('discord.js');
			const European = moment.utc(args[1], "HH:mm").tz('Europe/Paris');
			const Eastern = moment.utc(args[1], 'HH:mm').tz('America/New_York');
			const China = moment.utc(args[1], 'HH:mm').tz('Asia/Shanghai');
			const UTC = moment.utc(args[1], "HH:mm");
			const EmbedTimezone = new Discord.RichEmbed()
				.setColor('#ffffff')
				.setAuthor(message.author.username, message.author.avatarURL)
				.setThumbnail('http://i66.tinypic.com/2ng5xfs.png')
				// .setThumbnail('https://cdn.discordapp.com/icons/498332783282749440/35837a92b9c26b8c5937744753b83a06.png')
				.addField(UTC.zoneAbbr(), UTC.format("HH:mm"), true)
				.addField(Eastern.zoneName(), Eastern.format("HH:mm"), true)
				.addField(European.zoneName(), European.format("HH:mm"), true)
				.addField(China.zoneName(), China.format('HH:mm'), true)
				.setTimestamp()
				.setFooter('Made By ZeroWidthWhiteSpace#9999', 'https://cdn.discordapp.com/avatars/506893172052459521/3214c24c381bf5dbe538606c40f24349.png?size=128');
			message.channel.send(EmbedTimezone);
		}
	},
};
